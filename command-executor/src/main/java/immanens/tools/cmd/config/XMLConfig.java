package immanens.tools.cmd.config;

import immanens.tools.xml.DocumentUtils;
import immanens.tools.xml.XMLException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;

/**
 * Command executor XML configuration
 *
 * @author jmcau
 * @version 1.0
 */
public class XMLConfig implements IConfig {

    private int threadCount = 1;
    private int roundCount = 1;
    private int roundInterval = 60;
    private List<String> commands = new ArrayList<String>();
    private String touchFile = "/tmp/command-executor.touch";
    private int touchInterval = 5;

    @Override
    public int getThreadCount() {
        return this.threadCount;
    }

    @Override
    public void setThreadCount(int threadCount) {
        this.threadCount = threadCount;
    }

    @Override
    public int getRoundCount() {
        return this.roundCount;
    }

    @Override
    public void setRoundCount(int roundCount) {
        this.roundCount = roundCount;
    }

    @Override
    public int getRoundInterval() {
        return this.roundInterval;
    }

    @Override
    public void setRoundInterval(int roundInterval) {
        this.roundInterval = roundInterval;
    }

    @Override
    public List<String> getSubmitCommands() {
        return this.commands;
    }

    /**
     * Check command line arguments and build config
     *
     * @param args The command line arguments
     * @return {@link Config} Scalability test configuration
     * @exception {@link ArgumentException} raised if arguments not valid
     * @exception {@link ConfigException} raised if xml config not valid
     */
    public static IConfig build(final String[] args) throws ArgumentException, ConfigException {
        final IConfig config = new XMLConfig();

        // check arguments
        if (args.length == 0) {
            throw new ArgumentException("Missing required argument");
        }
        if (args.length != 1) {
            throw new ArgumentException("Wrong count of arguments");
        }
        File file = new File(args[0]);
        if (!file.exists()) {
            throw new ArgumentException("XML config file not found");
        }

        // load xml config
        Document document = null;
        try {
            document =  DocumentUtils.loadDocument(file);
        } catch (XMLException e) {
            throw new ConfigException(e.getMessage(), e);
        }

        // get thread count
        Element eThreadCount = null;
        try {
            eThreadCount = DocumentUtils.searchElement(document.getRootElement(), "thread-count");
        } catch (XMLException e) {
            throw new ConfigException(e.getMessage(), e);
        }
        if (eThreadCount == null) {
            throw new ConfigException("Missing <thread-count> element");
        }
        int threadCount = 0;
        try {
            threadCount = Integer.parseInt(eThreadCount.getTextTrim());
        } catch (NumberFormatException e) {
            throw new ConfigException("Mismatch value for thread count");
        }
        if (threadCount < 1 || threadCount > 1000) {
            throw new ConfigException("Bad value for thread count: between 1 and 1000");
        }
        config.setThreadCount(threadCount);

        // get round count
        Element eRoundCount = null;
        try {
            eRoundCount = DocumentUtils.searchElement(document.getRootElement(), "round-count");
        } catch (XMLException e) {
            throw new ConfigException(e.getMessage(), e);
        }
        if (eRoundCount == null) {
            throw new ConfigException("Missing <round-count> element");
        }
        int roundCount = 0;
        try {
            roundCount = Integer.parseInt(eRoundCount.getTextTrim());
        } catch (NumberFormatException e) {
            throw new ConfigException("Mismatch value for round count");
        }
        if (roundCount < 1 || roundCount > 100) {
            throw new ConfigException("Bad value for round count: between 1 and 100");
        }
        config.setRoundCount(roundCount);

        // get round interval
        Element eRoundInterval = null;
        try {
            eRoundInterval = DocumentUtils.searchElement(document.getRootElement(), "round-interval");
        } catch (XMLException e) {
            throw new ConfigException(e.getMessage(), e);
        }
        if (eRoundInterval == null) {
            throw new ConfigException("Missing <round-interval> element");
        }
        int roundInterval = 0;
        try {
            roundInterval = Integer.parseInt(eRoundInterval.getTextTrim());
        } catch (NumberFormatException e) {
            throw new ConfigException("Mismatch value for round interval");
        }
        if (roundInterval < 10 || roundInterval > 3600) {
            throw new ConfigException("Bad value for round interval: between 10 and 3600 seconds");
        }
        config.setRoundInterval(roundInterval);

        // get submit commands
        List<Element> eSubmitCommands = null;
        try {
            eSubmitCommands = DocumentUtils.searchElements(document.getRootElement(), "submit-commands/command");
        } catch (XMLException e) {
            throw new ConfigException(e.getMessage(), e);
        }
        if (eSubmitCommands.size() == 0) {
            throw new ConfigException("Missing <submit-commands>/<command> element");
        }
        for (Element eSubmitCommand : eSubmitCommands) {
            String command = eSubmitCommand.getTextTrim();
            config.getSubmitCommands().add(command);
        }

        return config;
    }
}
