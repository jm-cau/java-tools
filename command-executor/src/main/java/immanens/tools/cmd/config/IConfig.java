package immanens.tools.cmd.config;

import java.util.List;

/**
 * Command executor configuration interface
 *
 * @author jmcau
 * @version 1.0
 */
public interface IConfig {

    /**
     * Get thread count
     *
     * @return Thread count
     */
    int getThreadCount();

    /**
     * Set thread count
     *
     * @param threadCount Thread count
     */
    void setThreadCount(int threadCount);

    /**
     * Get round count
     *
     * @return Round count
     */
    int getRoundCount();

    /**
     * Set round count
     *
     * @param roundCount Round count
     */
    void setRoundCount(int roundCount);

    /**
     * Get round interval
     *
     * @return Round interval
     */
    int getRoundInterval();

    /**
     * Set round interval
     *
     * @param roundInterval Round interval
     */
    void setRoundInterval(int roundInterval);

    /**
     * Get submit commands
     */
    List<String> getSubmitCommands();
}
