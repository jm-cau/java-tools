package immanens.tools.cmd.config;

/**
* Signals any exception that occur during xml config loading/reading
*
* @author jmcau
* @version 1.0
*/
public class ConfigException extends Exception {

    public ConfigException() {
        super();
    }

    public ConfigException(String message) {
        super(message);
    }

    public ConfigException(Throwable cause) {
        super(cause);
    }

    public ConfigException(String message, Throwable cause) {
        super(message, cause);
    }
}
