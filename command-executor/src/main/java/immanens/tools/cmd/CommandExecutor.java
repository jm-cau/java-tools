package immanens.tools.cmd;

import immanens.tools.cmd.config.ConfigException;
import immanens.tools.cmd.config.ArgumentException;
import immanens.tools.cmd.config.IConfig;
import immanens.tools.cmd.config.XMLConfig;
import immanens.tools.io.FileTouch;
import immanens.tools.log.MultiLogger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.function.Consumer;
import java.util.logging.Level;

/**
* Execute a pool of console commands using threads
*
* @author jmcau
* @version 1.0
*/
public class CommandExecutor {

    private static String TOUCH_FILE = "/tmp/command-executor.touch";
    private static String LOG_TEXT_FILE = "/tmp/command-executor.log.txt";
    private static String LOG_XML_FILE = "/tmp/command-executor.log.xml";
    private static String DEFAULT_FORMAT = "[%1$tD][%1$tT] %5$s%n";

    /**
     * Main entry
     *
     * @param args The command line arguments
     */
    public static void main(String[] args) {

        // init logging
        MultiLogger logger = null;
        try {
            logger = new MultiLogger(CommandExecutor.class.getName(), CommandExecutor.DEFAULT_FORMAT);
            logger.addConsole(Level.FINEST);
            logger.addTextFile(CommandExecutor.LOG_TEXT_FILE, Level.INFO);
            logger.addXMLFile(CommandExecutor.LOG_XML_FILE, Level.INFO);
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (logger == null) {
            System.exit(1);
        }

        // build configuration
        IConfig config = null;
        try {
            logger.info("Command executor started");
            config = XMLConfig.build(args);
            logger.info("Configuration loaded");
            logger.info("> Thread count: "+config.getThreadCount());
            logger.info("> Round count: "+config.getRoundCount());
            logger.info("> Round interval: "+config.getRoundInterval()+" seconds");
            for (String submitCommand : config.getSubmitCommands()) {
                logger.info("> Submit command: "+submitCommand);
            }
            logger.fine(" ");
        } catch (ArgumentException e) {
            logger.warning(e.getMessage());
            CommandExecutor.printUsage();
        } catch (ConfigException e) {
            logger.severe(e);
        } catch (Exception e) {
            logger.severe(e);
            e.printStackTrace();
        }
        if (config == null) {
            System.exit(1);
        }

        int round = 0;
        long nextRound = System.currentTimeMillis();
        FileTouch toucher = new FileTouch(CommandExecutor.TOUCH_FILE, 30);

        // process rounds
        do {
            long roundTime = System.currentTimeMillis();
            if (roundTime < nextRound) {
                try {
                    toucher.touch();
                } catch (IOException e) {
                    logger.severe(e);
                    System.exit(1);
                }
                Thread.yield();
                continue;
            }

            nextRound = roundTime + (config.getRoundInterval() * 1000);
            round++;

            logger.info("Process round "+round+" started");

            ProcessBuilder processBuilder = new ProcessBuilder();
            ExecutorService resultExecutor = Executors.newSingleThreadExecutor();
            Set<Callable<String>> resultPool = new HashSet<Callable<String>>();

            // execute round commands
            try {
                for (int index=1; index <= config.getThreadCount(); index++) {
                    // set command to process
                    int commandIndex =  (new Random()).nextInt(config.getSubmitCommands().size());
                    String commandString = config.getSubmitCommands().get(commandIndex);
                    List<String> commandList = Arrays.asList(commandString.split("\\s+"));
                    processBuilder.command(commandList);

                    // launch command thread
                    logger.info("Launch tread "+index+": "+String.join(" ", processBuilder.command()));
                    Process process = processBuilder.start();

                    // add result process to pool
                    resultPool.add(new ResultProcess(process, index, commandString, logger));
                }
            } catch (IOException e) {
                logger.severe(e);
            }

            // execute result threads
            try {
                List<Future<String>> futures = resultExecutor.invokeAll(resultPool);
                for (Future<String> future : futures) {
                    String message = future.get();
                }
            } catch (InterruptedException|ExecutionException e) {
                logger.severe(e);
            }
            logger.info("Process round "+round+" ended");
        } while (round < config.getRoundCount());

        logger.info("Command executor ended");

        System.exit(0);
    }

    /**
     * Command result thread
     */
    private static class ResultProcess implements Callable {
        private Process process;
        private int index;
        private String command;
        private MultiLogger logger;

        /**
         * Constructor
         *
         * @param process Command process
         * @param index Thread index
         * @param command Executed command
         */
        public ResultProcess(final Process process, final int index, final String command, final MultiLogger logger) {
            this.process = process;
            this.index = index;
            this.command = command;
            this.logger = logger;
        }

        @Override
        public String call() throws Exception {
            String message = null;
            try {
                StringBuilder output = new StringBuilder();
                BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                output.append(this.command + "\n");
                String line;
                while ((line = reader.readLine()) != null) {
                    output.append(line + "\n");
                }
                int exitVal = this.process.waitFor();
                if (exitVal == 0) {
                    message = "Thread "+this.index+" completed";
                    this.logger.info(message);
                    this.logger.trace(output.toString());
                } else {
                    message = "Thread "+this.index+" failed!";
                    this.logger.warning(message);
                }
            } catch (InterruptedException e) {
                message = "Thread "+this.index+" raised an error:";
                this.logger.warning(message);
                this.logger.severe(e);
            }

            return message;
        }
    }

    /**
     * Print usage
     */
    private static void printUsage() {
        System.out.println("  ");
        System.out.println("Usage: java -jar command-executor-<version>.jar <xml-config>");
        System.out.println("  ");
    }
}
