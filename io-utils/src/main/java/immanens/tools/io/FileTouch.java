package immanens.tools.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
* Execute a touch on a file
*
* @author jmcau
* @version 1.0
*/
public class FileTouch {

    private File file;
    private int interval;
    private long nextTouch;

    /**
     * Constructor
     *
     * @param String Touch filename
     * @param interval Touch interval
     */
    public FileTouch(final String filename, final int interval) {
        this.file = new File(filename);
        this.interval = interval;
        this.nextTouch = System.currentTimeMillis();
    }

    /**
     * Touch file
     */
    public void touch() throws IOException {
        long touchTime = System.currentTimeMillis();
        if (touchTime >= this.nextTouch) {
            if (!this.file.exists()) {
                new FileOutputStream(this.file).close();
            }
            this.file.setLastModified(System.currentTimeMillis());
            this.nextTouch = touchTime + (this.interval * 1000);
        }
    }
}
