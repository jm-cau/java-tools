package immanens.tools.xml;

/**
* Signals any exception that occur during XML process.
*
* @author jmcau
* @version 1.0
*/
public class XMLException extends Exception {

	public XMLException() {
		super();
	}

	public XMLException(String message) {
		super(message);
	}

	public XMLException(Throwable cause) {
		super(cause);
	}

	public XMLException(String message, Throwable cause) {
		super(message, cause);
	}
}
