package immanens.tools.xml;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.URL;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.filter.Filters;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;

/**
* Utilities to process & explore a XML file
*
* @author jmcau
* @version 1.0
*/
public class DocumentUtils {

    static public Document loadDocument(final String filename) throws XMLException {
        return parseFile(new File(filename));
    }

    static public Document loadDocument(final File file) throws XMLException {
        return parseFile(file);
    }

    static public Document loadDocument(final URL url) throws XMLException {
        return parseFile(url);
    }

    static public Document loadDocument(final InputStream stream) throws XMLException {
        return parseFile(stream);
    }

    static public Document loadDocument(final Reader reader) throws XMLException {
        return parseFile(reader);
    }

    static private Document parseFile(final Object source) throws XMLException {
        try {
            SAXBuilder builder = new SAXBuilder();
            builder.setIgnoringElementContentWhitespace(true);
            Document document = null;
            if (source instanceof File) {
                document = builder.build((File) source);
            } else if (source instanceof URL) {
                document = builder.build((URL) source);
            } else if (source instanceof InputStream) {
                document = builder.build((InputStream) source);
            } else if (source instanceof Reader) {
                document = builder.build((Reader) source);
            }
            return document;
        } catch (Exception e) {
            throw new XMLException(e);
        }
    }

    static public void writeDocument(final Document document, final OutputStream out, final boolean pretty) throws XMLException {
        try {
            String charset = "utf-8";
            Format format = null;
            if (pretty) {
                format = Format.getPrettyFormat();
            } else {
                format = Format.getCompactFormat();
            }
            format.setEncoding(charset);
            format.setLineSeparator(System.getProperty("line.separator"));
            XMLOutputter outputter = new XMLOutputter(format);
            outputter.output(document, out);
        } catch (Exception e) {
            throw new XMLException(e);
        }
    }

    static public void writeDocument(final Document document, final Writer out, final boolean pretty) throws XMLException {
        try {
            String charset = "utf-8";
            Format format = null;
            if (pretty) {
              format = Format.getPrettyFormat();
            } else {
              format = Format.getCompactFormat();
            }
            format.setEncoding(charset);
            format.setLineSeparator(System.getProperty("line.separator"));
            XMLOutputter outputter = new XMLOutputter(format);
            outputter.output(document, out);
        } catch (Exception e) {
            throw new XMLException(e);
        }
    }

    static public void writeDocument(final Document document, final File file, final boolean pretty) throws XMLException {
        try {
            String charset = "utf-8";
            OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(file), charset);
            Format format = null;
            if (pretty) {
                format = Format.getPrettyFormat();
            } else {
                format = Format.getCompactFormat();
            }
            format.setEncoding(charset);
            format.setLineSeparator(System.getProperty("line.separator"));
            XMLOutputter outputter = new XMLOutputter(format);
            outputter.output(document, out);
        } catch (Exception e) {
            throw new XMLException(e);
        }
    }

    static public String toString(final Document document, final boolean pretty) throws XMLException {
        try {
            String charset = "utf-8";
            Format format = null;
            if (pretty) {
                format = Format.getPrettyFormat();
            } else {
                format = Format.getCompactFormat();
            }
            format.setEncoding(charset);
            format.setLineSeparator(System.getProperty("line.separator"));
            XMLOutputter outputter = new XMLOutputter(format);
            return outputter.outputString(document);
        } catch (Exception e) {
            throw new XMLException(e);
        }
    }

    static public Element addElement(final Element parent, final String name) throws XMLException {
        return addElement(parent, name, null);
    }

    static public Element addElement(final Element parent, final String name, final String text) throws XMLException {
        try {
            Element child = new Element(name);
            if (text != null) {
                child.setText(text);
            }
            parent.addContent(child);
            return child;
        } catch (Exception e) {
            throw new XMLException(e);
        }
    }

    static public Element searchElement(final Element element, final String xpath) throws XMLException {
        try {
            XPathFactory factory = XPathFactory.instance();
            XPathExpression<Element> expr = factory.compile(xpath, Filters.element());
            return expr.evaluateFirst(element);
        } catch (Exception e) {
            throw new XMLException(e);
        }
    }

    static public List<Element> searchElements(final Element element, final String xpath) throws XMLException {
        try {
            XPathFactory factory = XPathFactory.instance();
            XPathExpression<Element> expr = factory.compile(xpath, Filters.element());
            return expr.evaluate(element);
        } catch (Exception e) {
            throw new XMLException(e);
        }
    }
}