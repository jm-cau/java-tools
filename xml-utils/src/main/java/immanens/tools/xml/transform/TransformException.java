package immanens.tools.xml.transform;

/**
* Signals any exception that occur during XML transformation
*
* @author jmcau
* @version 1.0
*/
public class TransformException extends Exception {

	public TransformException() {
		super();
	}

	public TransformException(String message) {
		super(message);
	}

	public TransformException(Throwable cause) {
		super(cause);
	}

	public TransformException(String message, Throwable cause) {
		super(message, cause);
	}
}
