package immanens.tools.xml.transform;

import java.io.FilterWriter;
import java.io.IOException;
import java.io.Writer;

public class RemoveFirstNewLinesFilterWriter extends FilterWriter {

	enum State { IN_NEWLINE, IDENTITY }

	private State state;

	protected RemoveFirstNewLinesFilterWriter(Writer wOut) {
		super(wOut);
		this.state = State.IN_NEWLINE;
	}

	@Override
	public void write(char[] cbuf, int off, int len) throws IOException {
		switch (this.state) {
		case IN_NEWLINE :
			int i = off;
			while ((i < off+len) && (cbuf[i] == '\n')) i++;
			if (i == off + len) {
				// write nothing and stay in NEW LINE state
			} else {
				super.write(cbuf, i, len - (i - off));
				this.state = State.IDENTITY;
			}
			break;
		case IDENTITY :
		default:
			super.write(cbuf, off, len);
			break;
		}
	}

	@Override
	public void write(int c) throws IOException {
		switch (this.state) {
		case IN_NEWLINE :
			if (c != '\n') {
				super.write(c);
				this.state = State.IDENTITY;
			} else { // no newline at start so identity
				this.state = State.IN_NEWLINE;
			}
			break;
		case IDENTITY :
		default:
			super.write(c);
			break;
		}
	}

	@Override
	public void write(String str, int off, int len) throws IOException {
		switch (this.state) {
		case IN_NEWLINE :
			int i = off;
			while ((i < off+len) && (str.charAt(i) == '\n')) i++;
			if (i == off + len) {
				// write nothing and stay in NEW LINE state
			} else {
				super.write(str, i, len - (i - off));
				this.state = State.IDENTITY;
			}
			break;
		case IDENTITY :
		default:
			super.write(str, off, len);
			break;
		}
	}
}
