package immanens.tools.xml.transform;

import java.io.InputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.Properties;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.URIResolver;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class TransformDocument {

	private TransformerFactory xsltFactory;
	private Map<String, Source> xmlExtraSources;

	public TransformDocument(boolean saxon) {
		Properties props = System.getProperties();
		if (saxon) {
			props.put("javax.xml.transform.TransformerFactory", "net.sf.saxon.TransformerFactoryImpl");
		} else {
			props.put("javax.xml.transform.TransformerFactory", "org.apache.xalan.processor.TransformerFactoryImpl");
		}
		System.setProperties(props);
		this.xsltFactory = TransformerFactory.newInstance();
		this.xmlExtraSources = null;
	}

	public void transform(PrintWriter out, Source xmlSource, Map<String, Source> xmlExtraSources, String xsltURL)
			throws TransformException {
		try {
			this.xmlExtraSources = xmlExtraSources;
		  StreamSource xsltSource = new StreamSource(getXSLTStream(xsltURL));
			xsltSource.setSystemId(xsltURL);
			TransformResolver resolver = new TransformResolver(this, xsltURL);
			this.xsltFactory.setURIResolver(resolver);
			Result xhtmlResult = new StreamResult(new RemoveFirstNewLinesFilterWriter(out));
			Transformer transformer = this.xsltFactory.newTransformer(xsltSource);
			transformer.transform(xmlSource, xhtmlResult);
		}	catch (Exception e) {
			throw new TransformException(e);
		}
	}

	InputStream getXSLTStream(String xsltURL)
			throws MalformedURLException, IOException {
		try {
			URL url = new URL(xsltURL);
			return url.openStream();
		} catch (Exception ignored) {
			ClassLoader loader = this.getClass().getClassLoader();
			URL url = loader.getResource(xsltURL);
			return url.openStream();
		}
	}

	Source getDocumentSource(String id) {
		if (this.xmlExtraSources == null) {
			return null;
		}
		return this.xmlExtraSources.get(id);
	}

}

class TransformResolver implements URIResolver {

	private TransformDocument transformer;
	private String baseURL;

	public TransformResolver(TransformDocument transformer, String xsltURL) {
		this.transformer = transformer;
		this.baseURL = xsltURL;
		if (baseURL.endsWith(".xsl")) {
			int pos = baseURL.lastIndexOf("/");
			this.baseURL = baseURL.substring(0, pos+1);
		}
	}

	@Override
	public Source resolve(String href, String base) throws TransformerException {
		Source source = null;
		if (href.startsWith("document_")) {
			source = this.transformer.getDocumentSource(href);
			source.setSystemId(href);
		} else if (href.endsWith(".xsl")) {
			String xsltURL = this.baseURL + href;
			try {
			  InputStream xsltStream = this.transformer.getXSLTStream(xsltURL);
			  source = new StreamSource(xsltStream);
			  source.setSystemId(xsltURL);
			} catch (Exception e) {
			  throw new TransformerException(e);
			}
		} else {
			try {
				URL url = new URL(href);
				source = new StreamSource(url.openStream());
			} catch (MalformedURLException e) {
				throw new TransformerException(e);
			} catch (IOException e) {
				throw new TransformerException(e);
			}
			source.setSystemId(href);
		}
		return source;
	}
}