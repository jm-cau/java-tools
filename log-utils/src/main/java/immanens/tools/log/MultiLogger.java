package immanens.tools.log;

import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.PrintWriter;

import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
* Execute a touch on a file
*
* @author jmcau
* @version 1.0
*/
public class MultiLogger {

    private static String DEFAULT_FORMAT = "[%4$s] [%1$tc] %5$s%n";

    private Logger logger;

    public MultiLogger(final String logName) throws SecurityException, IOException {
        this(logName, MultiLogger.DEFAULT_FORMAT);
    }

    public MultiLogger(final String logName, final String format) throws SecurityException, IOException {
        this.logger = Logger.getLogger(logName);
        this.logger.setUseParentHandlers(false);
        this.logger.setLevel(Level.ALL);
        System.setProperty("java.util.logging.SimpleFormatter.format", format);
    }

    /**
     * Add a console
     */
    public void addConsole() throws SecurityException, IOException {
        addConsole(Level.ALL);
    }
    public void addConsole(final Level level) throws SecurityException, IOException {
        Handler handler = new ConsoleHandler();
        handler.setLevel(level);
        handler.setFormatter(new SimpleFormatter());
        this.logger.addHandler(handler);
    }

    /**
     * Add a text file
     */
    public void addTextFile(final String filepath) throws SecurityException, IOException {
        addTextFile(filepath, Level.ALL);
    }
    public void addTextFile(final String filepath, final Level level) throws SecurityException, IOException {
        Handler handler = new FileHandler(filepath);
        handler.setLevel(level);
        handler.setFormatter(new SimpleFormatter());
        this.logger.addHandler(handler);
    }

    /**
     * Add a XML file
     */
    public void addXMLFile(final String filepath) throws SecurityException, IOException {
        addXMLFile(filepath, Level.ALL);
    }
    public void addXMLFile(final String filepath, final Level level) throws SecurityException, IOException {
        Handler handler = new FileHandler(filepath);
        handler.setLevel(level);
        this.logger.addHandler(handler);
    }

    /**
     * Trace a message
     */
    public void trace(final String message) {
        System.out.println(message);
    }

    /**
     * Log a message
     */
    public void severe(final String message) {
        this.logger.severe(message);
    }
    public void severe(final Exception e) {
        this.logger.severe(e.getMessage());
        System.out.println(MultiLogger.getAllExceptionsTrace(e));
    }
    public void warning(final String message) {
        this.logger.warning(message);
    }
    public void info(final String message) {
        this.logger.info(message);
    }
    public void config(final String message) {
        this.logger.config(message);
    }
    public void fine(final String message) {
        this.logger.fine(message);
    }
    public void finer(final String message) {
        this.logger.finer(message);
    }
    public void finest(final String message) {
        this.logger.finest(message);
    }

    /**
     * Get exception trace
     */
    private static String getExceptionTrace(final Throwable error) {
        return MultiLogger.getExceptionTrace(error, 0);
    }
    private static String getExceptionTrace(final Throwable error, final int limit) {
        String trace = "No trace";
        if (error != null) {
            final CharArrayWriter caw = new CharArrayWriter();
            final PrintWriter pw = new PrintWriter(caw);
            error.printStackTrace(pw);
            trace = caw.toString();
            caw.close();
            pw.close();
            if (limit > 0) {
                trace = trace.substring(0, Math.min(limit, trace.length()));
            }
        }
        return trace;
    }

    /**
     * Get all exceptions trace
     */
    private static String getAllExceptionsTrace(final Throwable error) {
        return MultiLogger.getAllExceptionsTrace(error, 0);
    }
    private static String getAllExceptionsTrace(Throwable error, final int limit)
    {
        String trace = MultiLogger.getExceptionTrace(error, limit);
        if (error != null) {
            while (true) {
                error = error.getCause();
                if (error != null) {
                    trace += "\r\n"+MultiLogger.getExceptionTrace(error, limit);
                } else {
                    break;
                }
            }
        }
        return trace;
    }
}
